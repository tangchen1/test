
package com.ljqc.demo;

import com.ljqc.dao.mongo.repository.SnykRepository;
import com.ljqc.dao.mongo.repository.VulProjectRepository;
import com.ljqc.dao.mongo.repository.VulnerabilityRepository;
import com.ljqc.dao.mysql.primary.*;
import com.ljqc.domain.mysql.primary.FortifyTask;
import com.ljqc.entity.es.*;
import com.ljqc.service.*;

import com.ljqc.util.*;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.io.File;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LjqcApplicationTests
{
    @Autowired
    private EsSearchService esSearchService;
    @Autowired
    VulnerabilityRepository vulnerabilityRepository;
    @Autowired
    VulProjectRepository vulProjectRepository;
    @Autowired
    FortifyService fortifyService;
    @Autowired
    SnykRepository snykRepository;

    @Test
    public void testFortify()
    {
        List list = snykRepository.findByAffects("org.jenkins-ci.plugins:shelve-project-plugin");
        FortifyTask fortifyTask = new FortifyTask();
        String path = "C:\\Users\\40352\\Documents\\WeChat Files\\a30254848\\FileStorage\\File\\2020-08\\758\\758.xml";
        fortifyTask.setPath(path);
        fortifyService.addFortifyTask(fortifyTask);
    }

    @Test
    public void testJpa()
    {
        String url = "https://github.com/torvalds/linux";
        esSearchService.updateVul();

       /* List<VulProjectNew> projectvulsList = vulProjectRepository.findAll();
        for(VulProjectNew n : projectvulsList)
        {
            esSearchService.updateVul(n.getProject_url());
        }*/
        String id  = "6296790";
        String msg = "{\"high_vul_num\":0,\"mid_vul_num\":0,\"unk_vul_num\":0,\"mid_vul_list\":[],\"is_vul_able\":false,\"total_vul_num\":0,\"low_vul_list\":[],\"vul_list\":[],\"low_vul_num\":0,\"hig_vul_list\":[]}";
        String result = new HttpRequestUtil().sendPostRequest(msg, "http://10.10.201.2/v2/updateSubjectCNV?id=6296790&operate=all");



        ProjectVo version = null;// result.getProjects().iterator().next();
        //String msg = "{\"high_vul_num\":" + version.getHigh_vul_num() + ",\"mid_vul_num\":" + version.getMid_vul_num() + ",\"unk_vul_num\":" + version.getUnk_vul_num() + ",\"mid_vul_list\":" + version.getMid_vul_list() + ",\"is_vul_able\":true,\"total_vul_num\":" + version.getTotal_vul_num() + "," +
         //       "\"low_vul_list\":" + version.getLow_vul_list() + ",\"vul_list\":" + version.getVul_list() + ",\"low_vul_num\":" + version.getLow_vul_num() + ",\"hig_vul_list\":" + version.getHig_vul_list() + "}";
        System.out.println(msg);
    }

    @Test
    public void detectTest()
    {
        byte[] encrypt = null;
        try
        {
            encrypt = DESUtil.encrypt("wzy:121212", "test1234");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        System.out.println(DESUtil.toHexString(encrypt));
        System.out.println("------------------");
    }


    @Test
    public void getImgBinary()
    {
        File f = new File("C:\\Users\\40352\\Desktop\\image\\logo01.png");
        BufferedImage bi;
        try
        {
            bi = ImageIO.read(f);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bi, "png", baos);
            byte[] bytes = baos.toByteArray();
            String str = new sun.misc.BASE64Encoder().encodeBuffer(bytes).trim();
            System.out.println(str);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void fileTest()
    {
        String folder = "C:\\code\\front";
        TreeNode thisFile = new TreeNode();
        thisFile.setKey(0);
        thisFile.setLabel(new File(folder).getName());
        TreeNode fileTree = FileUtil.getFileTree(folder, thisFile, 0);
        System.out.println(123);
    }

}
